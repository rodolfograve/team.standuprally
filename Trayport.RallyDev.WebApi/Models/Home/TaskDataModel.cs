﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trayport.RallyDev.WebApi.Models.Home
{
   public class TaskDataModel
   {

      public static TaskDataModel CreateFrom(RallyTask source)
      {
         var result = new TaskDataModel()
            {
               Blocked = source.Blocked,
               BlockedReason = source.BlockedReason,
               Description = source.Description,
               Estimate = source.Estimate,
               FormattedId = source.FormattedID,
               LastUpdateDate = source.LastUpdatedAt,
               Name = source.Name,
               ObjectId = source.ObjectID,
               State = source.State,
               ToDo = source.ToDo,
               IsInProgress = source.State == "In-Progress",
               Rank = source.Rank
            };
         if (source.Owner != null)
         {
            result.OwnerId = source.Owner.ObjectID;
            result.OwnerName = source.OwnerName;
         }
         if (source.WorkProduct != null)
         {
            result.UserStoryId = source.WorkProduct.ObjectID;
         }
         return result;
      }

      private TaskDataModel()
      {
      }

      public long ObjectId { get; private set; }
      public string FormattedId { get; private set; }
      public string Name { get; private set; }
      public string Description { get; set; }
      public long? OwnerId { get; set; }
      public string OwnerName { get; private set; }
      public bool Blocked { get; private set; }
      public string BlockedReason { get; private set; }
      public decimal Estimate { get; private set; }
      public decimal ToDo { get; private set; }
      public string State { get; private set; }
      public DateTime LastUpdateDate { get; private set; }
      public bool IsInProgress { get; private set; }
      public decimal Rank { get; set; }

      public long? UserStoryId { get; set; }
      public string UserStoryFormattedId { get; set; }

   }
}