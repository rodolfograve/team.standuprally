﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trayport.RallyDev.WebApi.Models.Home
{
   public class IterationsViewModel
   {

      public long ProjectId { get; set; }

      public string ProjectName { get; set; }

      public IEnumerable<RallyIteration> Iterations { get; set; }

   }
}