﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trayport.RallyDev.WebApi.Models.Home
{
   public class IndexViewModel
   {

      public IEnumerable<RallyProject> Projects { get; set; }

   }
}