﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trayport.RallyDev.WebApi.Models.Home
{
   public class PrintIterationViewModel
   {

      public PrintIterationViewModel(IEnumerable<RallyUserStory> userStories, IEnumerable<RallyTask> tasks, Func<long, IEnumerable<RallyTest>> tests)
      {
         UserStories = userStories.Select(x => new UserStoryPrintModel()
            {
               ObjectId = x.ObjectID,
               FormattedId = x.FormattedID,
               Name = x.Name,
               EstimatedPoints = x.PlanEstimate
            }).ToArray();

         Tasks = tasks.Select(x => new TaskPrintModel()
            {
               FormattedId = x.FormattedID,
               Name = x.Name,
               EstimatedHours = x.Estimate,
               WorkingProductId = x.WorkProduct.ObjectID              
            }).ToArray();

         Tests = UserStories.SelectMany( x => tests(x.ObjectId))
                      .Where(x => !string.IsNullOrEmpty(x.PreConditions))
                      .Where(x=> x.WorkProduct!=null)
                      .Select(x => new TestCasePrintModel()
            {
               FormattedId = x.FormattedID,
               Given = x.PreConditions,
               When = x.ValidationInput,
               Then = x.ValidationExpectedResult,
               WorkingProductId = x.WorkProduct.ObjectID
            }).ToArray();

         foreach (var task in Tasks)
         {
            var userStory = userStories.FirstOrDefault(x => x.ObjectID == task.WorkingProductId);
            task.UserStoryFormattedId = userStory == null ? "????" : userStory.FormattedID;
         }

         foreach (var test in Tests)
         {
            var userStory = userStories.FirstOrDefault(x => x.ObjectID == test.WorkingProductId);
            test.UserStoryFormattedId = userStory == null ? "????" : userStory.FormattedID;
         }
      }

      public IEnumerable<UserStoryPrintModel> UserStories { get; private set; }

      public IEnumerable<TaskPrintModel> Tasks { get; private set; }

      public IEnumerable<TestCasePrintModel> Tests { get; private set; }

   }
}