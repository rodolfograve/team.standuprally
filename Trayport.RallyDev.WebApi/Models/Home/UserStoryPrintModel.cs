﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trayport.RallyDev.WebApi.Models.Home
{
   public class UserStoryPrintModel
   {

      public long ObjectId { get; set; }

      public string FormattedId { get; set; }

      public decimal EstimatedPoints { get; set; }

      public string Name { get; set; }

   }
}
