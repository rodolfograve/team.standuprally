﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Trayport.RallyDev.WebApi.Models.Home
{
   public class UserStoryDataModel
   {

      public static UserStoryDataModel CreateFrom(RallyUserStory source)
      {
         var result = new UserStoryDataModel()
            {
               Description = source.Description,
               FormattedId = source.FormattedID,
               LastUpdateDate = source.LastUpdatedAt,
               Name = source.Name,
               ObjectId = source.ObjectID,
               ScheduleState = source.ScheduleState,
               TaskEstimateTotal = source.TaskEstimateTotal,
               TaskRemainingTotal = source.TaskRemainingTotal,
               TaskStatus = source.TaskStatus,
               Rank = source.Rank
            };
         if (source.Owner != null)
         {
            result.OwnerId = source.Owner.ObjectID;
            result.OwnerName = source.OwnerName;
         }
         return result;
      }

      public UserStoryDataModel()
      {
         Tasks_Internal = new List<TaskDataModel>();
      }

      public long ObjectId { get; set; }
      public string FormattedId { get; private set; }
      public string ScheduleState { get; set; }
      public string OwnerName { get; private set; }
      public long OwnerId { get; private set; }
      public string Name { get; set; }
      public string Description { get; private set; }
      public decimal TaskEstimateTotal { get; private set; }
      public decimal TaskRemainingTotal { get; private set; }
      public DateTime LastUpdateDate { get; private set; }
      public string TaskStatus { get; private set; }
      public decimal Rank { get; set; }

      private IList<TaskDataModel> Tasks_Internal;
      public IEnumerable<TaskDataModel> Tasks
      {
         get { return Tasks_Internal; }
      }

      public void AddTask(TaskDataModel task)
      {
         Tasks_Internal.Add(task);
         task.UserStoryFormattedId = FormattedId;
         task.UserStoryId = ObjectId;
      }

      public UserStoryDataModel CopyOnlyWithTasksForUser(long userId, Func<TaskDataModel, bool> tasksFilter)
      {
         var result = new UserStoryDataModel()
            {
               Description = Description,
               FormattedId = FormattedId,
               LastUpdateDate = LastUpdateDate,
               Name = Name,
               ObjectId = ObjectId,
               ScheduleState = ScheduleState,
               TaskEstimateTotal = TaskEstimateTotal,
               TaskRemainingTotal = TaskRemainingTotal,
               TaskStatus = TaskStatus,
               Rank = Rank,
               OwnerId = OwnerId,
               OwnerName = OwnerName
            };
         foreach (var task in Tasks.Where(x => (userId == 0 || x.OwnerId == userId) && tasksFilter(x)))
         {
            result.AddTask(task);
         }
         return result;
      }

   }
}