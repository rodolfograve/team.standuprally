﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trayport.RallyDev.WebApi.Models.Home
{
    public class StandupViewModel
    {

        public StandupViewModel(string projectName, string iterationName, IEnumerable<RallyTask> tasks, IEnumerable<RallyUserStory> workProductsOrderedByRank)
        {
            ProjectName = projectName;
            IterationName = iterationName;

            var users = new Dictionary<long, UserDataModel>();
            var userStories = new Dictionary<long, UserStoryDataModel>();
            var today = DateTime.UtcNow.Date;
            foreach (var task in tasks.Where(x => x.Owner != null && x.WorkProduct != null))
            {
               UserDataModel user = null;
               if (!users.TryGetValue(task.Owner.ObjectID, out user))
               {
                  user = new UserDataModel(task.Owner.ObjectID, task.OwnerName);
                  users.Add(task.Owner.ObjectID, user);
               }

               UserStoryDataModel userStory = null;
               if (!userStories.TryGetValue(task.WorkProduct.ObjectID, out userStory))
               {
                  var rallyUserStory = workProductsOrderedByRank.FirstOrDefault(x => x.ObjectID == task.WorkProduct.ObjectID);

                  if (rallyUserStory == null)
                  {
                     userStories.Add(task.WorkProduct.ObjectID, new UserStoryDataModel()
                        {
                           ObjectId = task.WorkProduct.ObjectID,
                           Name = task.WorkProductName,
                           ScheduleState = ""
                        });
                     //throw new ApplicationException("Couldn't find the UserStory for task '" + task.FormattedID +
                     //                                 "' [" + task.ObjectID + "]");
                  }
                  else
                  {
                     userStory = UserStoryDataModel.CreateFrom(rallyUserStory);
                     userStories.Add(userStory.ObjectId, userStory);
                  }
               }

               if (userStory != null)
               {
                  userStory.AddTask(TaskDataModel.CreateFrom(task));
               }
            }

            foreach (var user in users.Values)
            {
                user.ProcessUserStories(userStories.Values);
            }

            var shuffledUsers = users.Values.ToList();
            shuffledUsers.Shuffle();
            var allUsersUser = new UserDataModel(0, "All Users");
            allUsersUser.ProcessUserStories(userStories.Values);
            shuffledUsers.Insert(0, allUsersUser);

            Users = shuffledUsers;
        }

        public string ProjectName { get; set; }

        public string IterationName { get; set; }

        public IEnumerable<UserDataModel> Users { get; private set; }

    }
}