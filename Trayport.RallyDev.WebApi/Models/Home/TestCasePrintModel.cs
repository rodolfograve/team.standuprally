﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trayport.RallyDev.WebApi.Models.Home
{
   public class TestCasePrintModel
   {
      public string FormattedId { get; set; }
      public long WorkingProductId { get; set; }
      public string UserStoryFormattedId { get; set; }
      public string Given { get; set; }
      public string When { get; set; }
      public string Then { get; set; }
   }
}