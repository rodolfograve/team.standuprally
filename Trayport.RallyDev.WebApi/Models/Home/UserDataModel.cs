﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Trayport.RallyDev.WebApi.Models.Home
{
    public class UserDataModel
    {

        public UserDataModel(long id, string name)
        {
           Id = id;
           Name = name;
           TotalPendingHours = 0;
           TotalCompletedHours = 0;
        }

        public long Id { get; private set; }

        public string Name { get; private set; }

        public IEnumerable<UserStoryDataModel> InProgressUserStories { get; private set; }
        public IEnumerable<UserStoryDataModel> UpdatedSinceYesterdayUserStories { get; private set; }
        public IEnumerable<UserStoryDataModel> AllUserStories { get; private set; }

        public DateTime Yesterday { get; private set; }
        public int TotalBreakDays { get; private set; }
        public decimal TotalPendingHours { get; private set; }
        public decimal TotalCompletedHours { get; private set; }
        public int TotalBlockedTasks { get; private set; }

        public void ProcessUserStories(IEnumerable<UserStoryDataModel> userStories)
        {

           var today = DateTime.Now.Date;
           var yesterday = DateTime.MinValue;
           var lastUpdate = DateTime.MinValue;

           foreach (var userStory in userStories)
           {
              foreach (var task in userStory.Tasks.Where(x => (Id == 0 || x.OwnerId == Id)))
              {
                 switch (task.State.ToLowerInvariant())
                 {
                    case "in-progress": { TotalPendingHours += task.ToDo; break; }
                    case "completed": { TotalCompletedHours += task.Estimate; break;}
                 }

                 if (task.Blocked) TotalBlockedTasks++;

                 if (task.State != "In-Progress" && task.LastUpdateDate.Date < today && task.LastUpdateDate.Date > yesterday)
                 {
                    yesterday = task.LastUpdateDate.Date;
                 }
                 if (task.LastUpdateDate < today && task.LastUpdateDate > lastUpdate)
                 {
                    lastUpdate = task.LastUpdateDate;
                 }
              }
           }
           Yesterday = yesterday;
           TotalBreakDays = (int)today.Subtract(lastUpdate).TotalDays;

           var allUserStories = new List<UserStoryDataModel>();
           var inProgressUserStories = new List<UserStoryDataModel>();
           var updatedSinceYesterdayUserStories = new List<UserStoryDataModel>();

           foreach (var userStory in userStories)
           {
               if (userStory.Tasks.Any(x => Id == 0 || x.OwnerId == Id))
               {
                  allUserStories.Add(userStory.CopyOnlyWithTasksForUser(Id, x => true));
               }
               if (userStory.Tasks.Any(x => (Id ==0 || x.OwnerId == Id) && x.State == "In-Progress"))
               {
                  inProgressUserStories.Add(userStory.CopyOnlyWithTasksForUser(Id, x => x.State == "In-Progress"));
               }
               if (userStory.Tasks.Any(x => (Id == 0 || x.OwnerId == Id) && x.State != "In-Progress" && x.LastUpdateDate >= Yesterday))
               {
                  updatedSinceYesterdayUserStories.Add(userStory.CopyOnlyWithTasksForUser(Id, x => x.LastUpdateDate >= Yesterday && x.State != "In-Progress"));
               }
           }

           InProgressUserStories = inProgressUserStories;
           UpdatedSinceYesterdayUserStories = updatedSinceYesterdayUserStories;
           AllUserStories = allUserStories;
        }
    }
}