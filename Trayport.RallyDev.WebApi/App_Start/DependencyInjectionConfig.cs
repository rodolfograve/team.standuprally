﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using Trayport.RallyDev.WebApi.Controllers;
using Autofac.Integration.WebApi;

namespace Trayport.RallyDev.WebApi
{
    public static class DependencyInjectionConfig
    {

        public static void ConfigureDI(ContainerBuilder builder)
        {
            builder.Register(x => new RallyDevGateway((string)HttpContext.Current.Session["username"], (string)HttpContext.Current.Session["password"]));
        }

    }
}