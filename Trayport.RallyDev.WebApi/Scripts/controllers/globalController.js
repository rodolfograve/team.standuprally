﻿'use strict';

function GlobalController($scope, $http) {

    $scope.authenticate = function () {
        var basicAuthenticationString = $scope.login + ":" + $scope.password;
		var encodedBasicAuthenticationString = 'Basic ' + btoa(basicAuthenticationString);
        $http({
            method: 'GET',
            url: 'https://rally1.rallydev.com/slm/webservice/v2.0/security/authorize',
            headers: { 'Authorization': encodedBasicAuthenticationString }
        }).success(function (data) {
            $scope.authenticationToken = data.OperationResult.SecurityToken;
			$scope.encodedBasicAuthenticationString = encodedBasicAuthenticationString;
			
			sessionStorage.authenticationToken = $scope.authenticationToken;
			sessionStorage.login = $scope.login;
			sessionStorage.encodedBasicAuthenticationString = encodedBasicAuthenticationString;
			
			setupDefaultAuthenticationHeaders($http, encodedBasicAuthenticationString);
        }).error(function (error) {
            alert("Authentication failed. Try again: " + error);
        });
    };
	
	$scope.logout = function() {
		sessionStorage.clear();
		$scope.login = null;
		$scope.aencodedBasicAuthenticationString = null;
		$scope.authenticationToken = null;
	};

    $scope.loadProjects = function () {
        $http.get("https://rally1.rallydev.com/slm/webservice/v2.0/project?workspace=https://rally1.rallydev.com/slm/webservice/v2.0/workspace/5647637346&query=&start=1&pagesize=20&key=" + $scope.authenticationToken)
			 .success(function (data) {
				$scope.projects = data.QueryResult.Results;
			 })
			 .error(function (error) {
             alert("Error loading projects: " + error);
        });
    };
	
	$scope.isDefined = function(v) {
		return v && v != "" && v != 'undefined';
	}
	
	$scope.isSessionValid = function() {
		return $scope.isDefined(sessionStorage.login) &&
			   $scope.isDefined(sessionStorage.authenticationToken) &&
			   $scope.isDefined(sessionStorage.encodedBasicAuthenticationString);
	}

	function setupDefaultAuthenticationHeaders($http, authenticationString) {
		$http.defaults.headers.common['Authorization'] = authenticationString;
	}

	// Run on controller load
	if ($scope.isSessionValid()) {
		$scope.login = sessionStorage.login;
		$scope.authenticationToken = sessionStorage.authenticationToken;
		$scope.encodedBasicAuthenticationString = sessionStorage.encodedBasicAuthenticationString;
		setupDefaultAuthenticationHeaders($http, $scope.encodedBasicAuthenticationString);
	}
	else {
		sessionStorage.login = "";
		sessionStorage.authenticationToken = "";
		sessionStorage.encodedBasicAuthenticationString = "";
		
		$scope.login = "rodolfo.grave@trayport.com";
		$scope.password = "";
	}
	
}

GlobalController.$inject = ['$scope', '$http'];
