﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Trayport.RallyDev.WebApi.Controllers
{
    public class ProjectsController : ApiController
    {
        public ProjectsController(RallyDevGateway gateway)
        {
            Gateway = gateway;
        }

        protected readonly RallyDevGateway Gateway;

        // GET api/values
        public IEnumerable<RallyProject> Get()
        {
            return Gateway.GetAllProjects();
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}