﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Trayport.RallyDev.WebApi.Models.Home;

namespace Trayport.RallyDev.WebApi.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(RallyDevGateway gateway)
        {
            Gateway = gateway;
        }

        protected readonly RallyDevGateway Gateway;

        public ActionResult ApiHelp()
        {
            return View("Index");
        }

        //[OutputCache(Duration = 600)]
        public ActionResult Index()
        {
            if (Gateway.UserName == null)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Index");
            }
            var model = new IndexViewModel()
            {
                Projects = Gateway.GetAllProjects().ToArray()
            };
            return View("Projects", model);
        }

        public ActionResult Iterations(long projectId, string projectName)
        {
            if (Gateway.UserName == null)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Index");
            }
            var model = new IterationsViewModel()
            {
                ProjectId = projectId,
                ProjectName = projectName,
                Iterations = Gateway.GetIterationsForProject(projectId).ToArray()
            };
            return View("Iterations", model);
        }

        public ActionResult Standup(long iterationId, string iterationName, long projectId, string projectName)
        {
            if (Gateway.UserName == null)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Index");
            }
            var tasks = Gateway.GetTasksInIteration(iterationId).ToArray();
            var userStories = Gateway.GetUserStoriesInIteration(iterationId);
            var defects = Gateway.GetDefectsInIteration(iterationId);
            var workProducts = userStories.Union(defects);


            var model = new StandupViewModel(projectName, iterationName, tasks, workProducts);

            return View(model);
        }

        public ActionResult ShowPrintIteration(long iterationId)
        {
            var userStories = Gateway.GetUserStoriesInIteration(iterationId);
            var defects = Gateway.GetDefectsInIteration(iterationId);
            var workProducts = userStories.Union(defects);
            var tasks = Gateway.GetTasksInIteration(iterationId);
            return View("PrintIteration", new PrintIterationViewModel(workProducts, tasks, Gateway.GetTestsInStory));
        }

        public ActionResult Leap()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}
