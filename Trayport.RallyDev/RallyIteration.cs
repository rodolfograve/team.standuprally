﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trayport.RallyDev
{
   public class RallyIteration
   {

      public static RallyIteration CreateFrom(dynamic rallyObject)
      {
         var result = new RallyIteration()
         {
            OriginalRallyObject = rallyObject
         };
         Extensions.CopyFromDynamic(result, rallyObject);
         if (result.EndDate != null)
         {
             result.EndAt = DateTime.Parse(result.EndDate);
         }
         return result;
      }

      public dynamic OriginalRallyObject { get; set; }

      public long ObjectID { get; set; }

      public string Name { get; set; }

      public string EndDate { get; set; }

      public DateTime EndAt { get; set; }

   }
}
