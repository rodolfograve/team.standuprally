﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trayport.RallyDev
{
   public class RallyObjectReference
   {

      public static long ParseRef(string refValue)
      {
            var fragmentStart = refValue.LastIndexOf('/');
            var fragmentEnd = refValue.LastIndexOf('.');
            var value = refValue.Substring(fragmentStart + 1, fragmentEnd - fragmentStart - 1);
            return Convert.ToInt64(value);
      }

      public static RallyObjectReference CreateFrom(dynamic rallyObject)
      {
         var result = new RallyObjectReference();
         Extensions.CopyFromDynamic(result, rallyObject);
         if (result.ObjectID == 0 && result._ref != null)
         {
            result.ObjectID = ParseRef(result._ref);
         }
         result.OriginalRallyObject = rallyObject;
         return result;
      }

      public dynamic OriginalRallyObject { get; set; }

      public string _ref { get; set; }

      public string _refObject { get; set; }

      public string _type { get; set; }

      public long ObjectID { get; set; }

   }
}
