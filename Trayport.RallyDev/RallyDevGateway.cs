﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rally.RestApi;
using Rally.RestApi.Response;

namespace Trayport.RallyDev
{
   public class RallyDevGateway
   {

      public RallyDevGateway(string userName, string password)
      {
         UserName = userName;
         Password = password;
         Api = new RallyRestApi(UserName, Password, webServiceVersion: "1.41");
      }

      public readonly string UserName;
      protected readonly string Password;
      protected readonly RallyRestApi Api;

      public bool CanConnect()
      {
         var result = Api.GetCurrentUser("UserName");
         return result.UserName == UserName;
      }

      public IEnumerable<RallyTest> GetTestsInStory(long storyId)
      {
         var request = new Request("testcase");
         request.Query = new Query("WorkProduct.ObjectID", Query.Operator.Equals, storyId.ToString());
         request.Fetch = new List<string>() { "true" };
         var results = Api.Query(request).Results;
         return results.Select(RallyTest.CreateFrom);
      }

      public IEnumerable<RallyTask> GetTasksInIteration(long iterationId)
      {
         var request = new Request("task");
         request.Query = new Query("Iteration.ObjectID", Query.Operator.Equals, iterationId.ToString());
         request.Order = "LastUpdateDate DESC";
         request.Fetch = new List<string>() { "true" };
         var result = Api.Query(request);
         var tasks = result.Results;

         return tasks.Select(RallyTask.CreateFrom);
      }

      public IEnumerable<RallyUserStory> GetUserStoriesInIteration(long iterationId)
      {
         var request = new Request("hierarchicalrequirement");
         request.Query = new Query("Iteration.ObjectID", Query.Operator.Equals, iterationId.ToString());
         request.Order = "Rank DESC";
         request.Fetch = new List<string>() { "true" };
         var result = Api.Query(request);
         var userStories = result.Results;

         return userStories.Select(RallyUserStory.CreateFrom);
      }

      public IEnumerable<RallyUserStory> GetDefectsInIteration(long iterationId)
      {
         var request = new Request("defect");
         request.Query = new Query("Iteration.ObjectID", Query.Operator.Equals, iterationId.ToString());
         request.Order = "Rank DESC";
         request.Fetch = new List<string>() { "true" };
         var result = Api.Query(request);
         var userStories = result.Results;

         return userStories.Select(RallyUserStory.CreateFrom);
      }

      public IEnumerable<RallyUser> GetAllUsers()
      {
         var request = new Request("user");
         var result = Api.Query(request);
         var users = result.Results;
         return users.Select(RallyUser.CreateFrom);
      }

      public IEnumerable<RallyProject> GetAllProjects()
      {
         var request = new Request("project");
         request.Fetch = new List<string>() { "ObjectID", "Name"};
         request.Order = "Name ASC";
         var result = Api.Query(request);
         var users = result.Results;
         return users.Select(RallyProject.CreateFrom);
      }

      public RallyProject GetProject(long projectId)
      {
         var request = new Request("project");
         request.Query = new Query("ObjectID", Query.Operator.Equals, projectId.ToString());
         request.Fetch = new List<string>() { "ObjectID", "Name", "TeamMembers" };
         var results = Api.Query(request).Results;
         if (results.Any())
         {
            return (RallyProject) RallyProject.CreateFrom(results.First());
         }
         else
         {
            return null;
         }
      }

      public IEnumerable<RallyUser> GetTeamMembers(long projectId)
      {
         var request = new Request("user");
         var project = GetProject(projectId);
         foreach (var teamMember in project.TeamMembers)
         {
            var query = new Query("ObjectID", Query.Operator.Equals, teamMember.ObjectID.ToString());
            if (request.Query == null)
            {
               request.Query = query;
            }
            else
            {
               request.Query = request.Query.Or(query);
            }
         }
         var result = Api.Query(request);
         var users = result.Results;
         return users.Select(RallyUser.CreateFrom);
      }

      public IEnumerable<RallyIteration> GetIterationsForProject(long projectId)
      {
         var request = new Request("iteration");
         request.Query = new Query("Project.ObjectID", Query.Operator.Equals, projectId.ToString());
         request.Fetch = new List<string>() { "ObjectID", "Name", "EndDate" };
         request.Order = "EndDate DESC";
         var results = Api.Query(request).Results;
         return results.Select(RallyIteration.CreateFrom);
      }
   }
}
