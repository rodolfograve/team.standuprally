﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trayport.RallyDev
{
   public class RallyWorkProduct
   {
      public static RallyWorkProduct CreateFrom(dynamic rallyObject)
      {
         return new RallyWorkProduct()
         {
            OriginalRallyObject = rallyObject
         };
      }

      public dynamic OriginalRallyObject { get; set; }

   }
}
