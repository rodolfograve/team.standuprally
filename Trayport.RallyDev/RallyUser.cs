﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trayport.RallyDev
{
   public class RallyUser
   {

      public static RallyUser CreateFrom(dynamic rallyObject)
      {
         var result = new RallyUser();
         result.OriginalRallyObject = rallyObject;
         Extensions.CopyFromDynamic(result, rallyObject);
         return result;
      }

      public dynamic OriginalRallyObject { get; set; }

      public long ObjectID { get; set; }
      public string Department { get; set; }
      public bool Disabled { get; set; }
      public string DisplayName { get; set; }
      public string EmailAddress { get; set; }
      public string Role { get; set; }
      public string UserName { get; set; }

      public bool IsVisible { get; set; }
   }
}
