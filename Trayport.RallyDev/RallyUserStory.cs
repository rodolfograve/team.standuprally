﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trayport.RallyDev
{
   public class RallyUserStory
   {

      public static RallyUserStory CreateFrom(dynamic rallyObject)
      {
         var result = new RallyUserStory();
         Extensions.CopyFromDynamic(result, rallyObject);

         result.OriginalRallyObject = rallyObject;
         if (result.Owner != null)
         {
            result.OwnerName = result.Owner.OriginalRallyObject["_refObjectName"];
         }
         if (!string.IsNullOrEmpty(result.LastUpdateDate))
         {
            result.LastUpdatedAt = DateTime.Parse(result.LastUpdateDate);
         }
         return result;
      }

      public dynamic OriginalRallyObject { get; set; }

      public long ObjectID { get; set; }
      public string Description { get; set; }
      public string Name { get; set; }
      public string Notes { get; set; }
      public RallyObjectReference Owner { get; set; }
      public decimal TaskEstimateTotal { get; set; }
      public RallyObjectReference Iteration { get; set; }
      public RallyObjectReference Project { get; set; }
      public decimal Rank { get; set; }
      public RallyObjectReference Release { get; set; }
      public string ScheduleState { get; set; }
      public string TaskStatus { get; set; }
      public decimal TaskRemainingTotal { get; set; }
      public IEnumerable<RallyObjectReference> Changesets { get; set; }
      public string FormattedID { get; set; }
      public string LastUpdateDate { get; set; }
      public decimal PlanEstimate { get; set; }

      public string OwnerName { get; set; }

      public DateTime LastUpdatedAt { get; private set; }
   }
}
