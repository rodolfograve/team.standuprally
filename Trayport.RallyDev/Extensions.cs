﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rally.RestApi;

namespace Trayport.RallyDev
{
   public static class Extensions
   {
         public static void CopyFromDynamic(this object target, DynamicJsonObject source)
         {
            var typeOfDynamicObject = typeof (DynamicJsonObject);
            var typeOfArrayList = typeof (ArrayList);
            var targetType = target.GetType();

            foreach (var field in source.Fields)
            {
               try
               {
                  var propertyInfo = targetType.GetProperty(field);
                  if (propertyInfo != null)
                  {
                     var fieldValue = source[field];
                     if (fieldValue != null)
                     {
                        var fieldType = fieldValue.GetType();
                        if (typeOfDynamicObject.IsAssignableFrom(fieldType))
                        {
                           var rallyObjectReference = RallyObjectReference.CreateFrom(fieldValue);
                           propertyInfo.SetValue(target, rallyObjectReference, null);
                        }
                        else if (typeOfArrayList.IsAssignableFrom(fieldType))
                        {
                           var propertyValue = ((ArrayList) fieldValue).Cast<DynamicJsonObject>().Select(RallyObjectReference.CreateFrom).ToArray();
                           propertyInfo.SetValue(target, propertyValue, null);
                        }
                        else
                        {
                           propertyInfo.SetValue(target, fieldValue, null);
                        }
                     }
                  }
               }
               catch (Exception ex)
               {
                  throw new InvalidOperationException("Error mapping property '" + field + "' from dynamic object to '" + targetType.Name + "'", ex);
               }
            }
         }

         static readonly Random Random = new Random();
         public static void Shuffle<T>(this IList<T> list)
         {
            int n = list.Count;
            while (n > 1)
            {
               n--;
               int k = Random.Next(n + 1);
               T value = list[k];
               list[k] = list[n];
               list[n] = value;
            }
         }

         public static void ForEach<T>(this IEnumerable<T> target, Action<T> action)
         {
            if (target != null)
            {
               foreach (var e in target)
               {
                  action(e);
               }
            }
         }
   }
}
