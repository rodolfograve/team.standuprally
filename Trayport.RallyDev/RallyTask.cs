﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using Rally.RestApi;

namespace Trayport.RallyDev
{
   public class RallyTask
   {

      public static RallyTask CreateFrom(dynamic rallyObject)
      {
         var result = new RallyTask();
         Extensions.CopyFromDynamic(result, rallyObject);

         result.OriginalRallyObject = rallyObject;
         if (result.Owner != null)
         {
             result.OwnerName = result.Owner.OriginalRallyObject["_refObjectName"];
         }
         if (result.WorkProduct != null)
         {
             result.WorkProductType = result.WorkProduct._type == "HierarchicalRequirement" ? "User Story" : result.WorkProduct._type;
             result.WorkProductName = result.WorkProduct.OriginalRallyObject["_refObjectName"];
         }
         if (!string.IsNullOrEmpty(result.LastUpdateDate))
         {
            result.LastUpdatedAt = DateTime.Parse(result.LastUpdateDate);
         }
         return result;
      }

      public dynamic OriginalRallyObject { get; set; }

      public long ObjectID { get; set; }
      public string CreationDate { get; set; }
      public string Description { get; set; }
      public string Name { get; set; }
      public string Notes { get; set; }
      public RallyObjectReference Owner { get; set; }
      public bool Ready { get; set; }
      public IEnumerable<RallyObjectReference> Tags { get; set; }
      public bool Blocked { get; set; }
      public string BlockedReason { get; set; }
      public decimal Estimate { get; set; }
      public RallyObjectReference Iteration { get; set; }
      public RallyObjectReference Project { get; set; }
      public decimal Rank { get; set; }
      public RallyObjectReference Release { get; set; }
      public string State { get; set; }
      public int TaskIndex { get; set; }
      public decimal ToDo { get; set; }
      public RallyObjectReference WorkProduct { get; set; }
      public IEnumerable<RallyObjectReference> Changesets { get; set; }
      public string FormattedID { get; set; }
      public string LastUpdateDate { get; set; }
      public RallyObjectReference RevisionHistory { get; set; }
      public decimal Actuals { get; set; }

      public string OwnerName { get; set; }
      public string WorkProductName { get; set; }
      public string WorkProductType { get; set; }

      public DateTime LastUpdatedAt { get; private set; }
   }
}
