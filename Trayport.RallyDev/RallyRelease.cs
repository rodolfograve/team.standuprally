﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trayport.RallyDev
{
   public class RallyRelease
   {
      public static RallyRelease CreateFrom(dynamic rallyObject)
      {
         return new RallyRelease()
         {
            OriginalRallyObject = rallyObject
         };
      }

      public dynamic OriginalRallyObject { get; set; }

   }
}
