﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trayport.RallyDev
{
   public class RallyTest
   {
      public static RallyTest CreateFrom(dynamic rallyObject)
      {
         var result = new RallyTest();
         Extensions.CopyFromDynamic(result, rallyObject);

         result.OriginalRallyObject = rallyObject;
         return result;
      }

      public dynamic OriginalRallyObject { get; set; }
      public string PreConditions { get; set; }
      public string ValidationInput { get; set; }
      public string ValidationExpectedResult { get; set; }
      public string FormattedID { get; set; }
      public string UserStoryID { get; set; }
      public RallyObjectReference WorkProduct { get; set; }
   }
}
