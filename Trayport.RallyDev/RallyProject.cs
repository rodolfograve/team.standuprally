﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rally.RestApi;

namespace Trayport.RallyDev
{
   public class RallyProject
   {

      public static RallyProject CreateFrom(dynamic rallyObject)
      {
         var result = new RallyProject()
            {
               OriginalRallyObject = rallyObject
            };
         Extensions.CopyFromDynamic(result, rallyObject);
         return result;
      }

      public dynamic OriginalRallyObject { get; set; }

      public long ObjectID { get; set; }

      public string Name { get; set; }

      public string Description { get; set; }

      public IEnumerable<RallyObjectReference> Iterations { get; set; }

      public IEnumerable<RallyObjectReference> Children { get; set; }

      public IEnumerable<RallyObjectReference> TeamMembers { get; set; }

   }
}
