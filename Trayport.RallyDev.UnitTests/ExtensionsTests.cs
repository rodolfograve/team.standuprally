﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rally.RestApi;
using FluentAssertions;
using System.Collections;

namespace Trayport.RallyDev.UnitTests
{
   [TestClass]
   public class ExtensionsTests
   {

      [TestMethod]
      public void Should_map_all_properties_but_DynamicJsonObject()
      {
         var properties = new Dictionary<string, object>()
            {
               {"ObjectID", 10},
               {"CreationDate", "2013/12/12"},
               {"Null", null},
               {"Iteration", new DynamicJsonObject()},
               {"Changesets", new ArrayList() { new DynamicJsonObject() }}
            };
         var source = new DynamicJsonObject(properties);

         var result = new RallyTask();

         Extensions.CopyFromDynamic(result, source);
         result.ObjectID.Should().Be(10);
         result.CreationDate.Should().Be("2013/12/12");
         result.Iteration.Should().BeOfType<RallyObjectReference>();
         result.Changesets.Should().NotBeNull();
         result.Changesets.Count().Should().Be(1);
      }

   }
}
