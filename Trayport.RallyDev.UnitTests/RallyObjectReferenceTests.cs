﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace Trayport.RallyDev.UnitTests
{
   [TestClass]
   public class RallyObjectReferenceTests
   {

      [TestMethod]
      public void Should_parse_the_ObjectID_from_ref()
      {
         var refValue = "https://rallydev.com/asdasdas/user/123123.js";
         var result = RallyObjectReference.ParseRef(refValue);
         result.Should().Be(123123);
      }

   }
}
