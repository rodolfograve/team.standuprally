﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rally.RestApi;
using FluentAssertions;

namespace Trayport.RallyDev.UnitTests
{
   [TestClass]
   public class RallyDevGatewayTests
   {

      #region Hidden

      private const string Password = "rodo820617";
#endregion

      private const long TestProjectId = 10602877338;
      private const long TestIterationId = 5767534269;

      private RallyDevGateway BuildGateway()
      {
         var result = new RallyDevGateway("rodolfo.grave@trayport.com", Password);
         return result;
      }

      [TestMethod]
      public void Should_connect_and_return_tasks_in_an_iteration()
      {
         var gateway = BuildGateway();
         var result = gateway.GetTasksInIteration(TestIterationId).ToArray();
      }

      [TestMethod]
      public void Should_connect_and_return_UserStories_in_an_iteration()
      {
         var gateway = BuildGateway();
         var result = gateway.GetUserStoriesInIteration(TestIterationId).ToArray();
      }

      [TestMethod]
      public void Should_connect_and_return_Defects_in_an_iteration()
      {
         var gateway = BuildGateway();
         var result = gateway.GetDefectsInIteration(TestIterationId).ToArray();
      }

      [TestMethod]
      public void Should_connect_and_return_users()
      {
         var gateway = BuildGateway();
         var result = gateway.GetAllUsers().ToArray();
         var first = result.First();
      }

      [TestMethod]
      public void Should_connect_and_return_projects()
      {
         var gateway = BuildGateway();
         var result = gateway.GetAllProjects().ToArray();
         var first = result.First();
         first.ObjectID.Should().NotBe(0);
      }

      [TestMethod]
      public void Should_connect_and_return_a_project_by_id()
      {
         var gateway = BuildGateway();
         var result = gateway.GetProject(TestProjectId);
         result.Should().NotBeNull();
      }

      [TestMethod]
      public void Should_connect_and_return_the_list_of_members_for_a_project()
      {
         var gateway = BuildGateway();
         var result = gateway.GetTeamMembers(TestProjectId).ToArray();
         result.Should().NotBeNull();
         result.Any().Should().BeTrue();
      }

      [TestMethod]
      public void Should_connect_and_return_the_list_of_iterations_of_a_project()
      {
         var gateway = BuildGateway();
         var result = gateway.GetIterationsForProject(TestProjectId).ToArray();
         result.Should().NotBeNull();
         result.Any().Should().BeTrue();
      }

   }
}
